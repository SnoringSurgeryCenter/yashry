<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('settings')->insert([
            'name'  => 'Taxes',
            'value' => '14',
            'value_type' => 'Percentage'
        ]);

    }
}
