<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $products =[
            [
                'id'   => 1,
                'name' => 'T-shirt',
                'price' => 10.99
            ],
            [
                'id'   => 2,
                'name' => 'Pants',
                'price' => 14.99
            ],
            [
                'id'   => 3,
                'name' => 'Jacket',
                'price' => 19.99
            ],
            [
                'id'   => 4,
                'name' => 'Shoes',
                'price' => 24.99
            ]
        ];
        foreach($products as $product){
            Product::create($product);
        }
    }
}
