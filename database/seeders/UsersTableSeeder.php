<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Cart;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
                'id'   => 1,
                'name' => 'Ahmed Habashy',
                'email' => 'ahmed@yashry.com',
                'password' => 'Ahmed@123'
        ]);
        Cart::create([
            'user_id' => 1
        ]);
    }
}
