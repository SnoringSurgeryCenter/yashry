<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $currencies = [
            [
                'name'  => 'Egyption Pounds',
                'code'  => 'EGP',
                'rate'  => 15.71
            ],[
                'name'  => 'Dollar',
                'code'  => 'USD',
                'rate'  => 1
            ]
        ];
        foreach($currencies as $currency)
        Currency::create($currency);
    }
}
