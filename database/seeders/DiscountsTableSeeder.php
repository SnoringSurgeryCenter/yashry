<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Discount;

class DiscountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $discounts = [
            [            
                'id'   => 1,
                'name' => 'Shoes are on 10%',
                'value' => 10,
                'value_type' => 'Percentage',
                'product_id' => 4,
                'count_product' => 1, 
                'discounted_product_id'    => 4,
                'is_available'      => 1
            ],
            [
                'id'   => 2,
                'name' => 'Buy two t-shirts and get a jacket half its price',
                'value' => 50,
                'value_type' => 'Percentage',
                'product_id' => 1,
                'count_product' => 2, 
                'discounted_product_id'    => 3,
                'is_available'      => 1
            ]
        ];
        foreach($discounts as $discount){
            Discount::create($discount);
        }
    }
}
