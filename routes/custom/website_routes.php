<?php 
# non authenticated admin routes
Route::post('login', 'AuthController@login')->name('admin.panel.login');

# authenticated admins only
Route::group([
    'middleware' => [
        'auth:api'
    ]
], function () {
    Route::delete('logout', 'AuthController@logout')->name('admin.panel.logout');
    Route::resource('products',ProductController::class, ['only'=> ['index']]);
    Route::resource('cart-products',CartProductController::class);
    Route::resource('currencies', CurrencyController::class, ['only'=> ['index']]);
    Route::resource('bills', BillController::class, ['only'=> ['store']]);

});