<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


# admin panel api routes
Route::group([
    'namespace' => 'Website',
    'prefix'    => 'yashry',
], function () {
    require_once base_path('routes/custom/website_routes.php');
});
