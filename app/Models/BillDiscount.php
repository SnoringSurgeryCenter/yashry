<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class BillDiscount extends Model
{
    use HasFactory;
    protected $table = 'bill_discounts';


    protected $fillable = [
        'bill_id',
        'discount_id',
        'value'
    ];
    ## Relations
    public function bill()
    {
        return $this->belongsTo(Bill::class, 'bill_id');
    }
    
    public function discount()
    {
        return $this->belongsTo(discount::class, 'discount_id');
    }
    ## Getters & Setters
    ## Query Scope Methods
   
    ## Other Methods
}
