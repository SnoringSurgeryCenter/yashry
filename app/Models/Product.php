<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
    ];

    ## Relations

    public function carts()
    {
        return $this->belongsToMany(Cart::class,'cart_products', 'product_id', 'cart_id')->withPivot('count');
    }

    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class);
    }


    public function discounts()
    {
        return $this->HasMany(Discount::class, 'product_id');
    }
    ## Query Scope Methods
    public function scopeAvailable($query,$discounted_product_id){
        return $query->where('id', $discounted_product_id);
    }
   
    ## Other Methods
    ## Getters & Setters
    public function getEligableDiscount($count = 0){ //should contaib the logic of choosing the eligable discount
       
        if($count> 0){
        return optional($this->discounts()->where('count_product', $count)->available()->orderBy('value','DESC'))->first();
        

        }
        else
        return optional($this->discounts()->available()->orderBy('value','DESC'))->first();
    }
}
