<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'value_type', //enum ->Amount , Percentage
        'product_id',
        'count_product',

        'discounted_product_id',
        'is_available',
    ];

    ## Relations

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    public function discountedProduct()
    {
        return $this->belongsTo(Product::class, 'discounted_product_id');
    }

    ## Getters & Setters
    ## Query Scope Methods
    public function scopeAvailable($query){
        return $query->where('is_available',1);
    }
    
    ## Other Methods
    public function getDiscountedAmount($price){
        if($this->value_type == "Percentage"){
            return $price*( $this->value / 100);
        }else
            return $price = $this->value;
    }
}
