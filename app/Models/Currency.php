<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'code',
        'rate'
    ];

    ## Relations
    ## Getters & Setters
    ## Query Scope Methods
   
    ## Other Methods
    public function change($amount){
        return $this->rate * $amount;
    }
}
