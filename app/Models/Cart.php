<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Auth;
class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
    ];
    protected $with =[
        //'products'
    ];
    
    private static $instance = NULL;

    protected function __construct() { }

    public static function getInstance() : Cart
    {
       if (self::$instance == null)
    {
      self::$instance = Auth::user()->cart;
    }
 
    return self::$instance;
    }
    ## Relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'cart_products', 'cart_id', 'product_id')->withPivot('count');
    }
    ## Getters & Setters
    ## Query Scope Methods
   
    ## Other Methods
}
