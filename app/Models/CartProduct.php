<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CartProduct extends Model
{
    use HasFactory;
    protected $table = 'cart_products';


    protected $fillable = [
        'cart_id',
        'product_id',
        'count'
    ];
    ## Relations
    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
    ## Getters & Setters
    ## Query Scope Methods
   
    ## Other Methods
}
