<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class Setting extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'value_type',
    ];
    ## Relations
    ## Getters and Setters

    ## Scopes

    ## Other methods
    public static function key($key)
    {
        return self::where('name', $key)->first();
    }
    
}
