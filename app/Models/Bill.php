<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bill extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'currency_id',
        'sub_total',
        'taxes',
        'total'
    ];
    ## Relations
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
    public function discounts()
    {
        return $this->belongsToMany(Discount::class, 'bill_discounts', 'bill_id', 'discount_id')->withPivot('value');
    }
    public function billDiscounts()
    {
        return $this->hasMany(BillDiscount::class, 'bill_id');
    }
    ## Getters & Setters
    ## Query Scope Methods
   
    ## Other Methods
    public function deductDiscountAmount($discountedamount){
        $this->total = $this->total - $discountedamount;
        $this->save();
    }
}
