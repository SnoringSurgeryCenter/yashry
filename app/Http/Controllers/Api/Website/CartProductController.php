<?php

namespace App\Http\Controllers\Api\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Website\CartProductResource;
use App\Http\Requests\Api\Website\CartProductStoreRequest;
use App\Http\Requests\Api\Website\CartProductUpdateRequest;
use App\Models\CartProduct;
use App\Models\Cart;
use Auth;

class CartProductController extends Controller
{
    public function index(){ // could add filter here in the future
        $cart = Cart::getInstance();
        return  CartProductResource::collection($cart->cartProducts()->get());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store( CartProductStoreRequest $request)
    {
        $cart = $request->storeCartProduct();
        return response([
            'message' => 'Added successfully',
            'cart' =>  CartProductResource::collection($cart->cartProducts()->get()),
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function update( CartProductUpdateRequest $request)
    {
        $cart = $request->updateCartProduct();
        return response([
            'message' => 'Updated successfully',
            'cart' =>  CartProductResource::collection($cart->cartProducts()->get()),
        ]);
    }

    public function destroy( $id )
    {
        $cart_product = CartProduct::FindorFail($id);
        $cart_product->delete();
        $cart = Cart::getInstance()->refresh();
        return response([
            'message' => 'Deleted successfully',
            'cart' =>  CartProductResource::collection($cart->cartProducts()->get()),
        ]);
    }


}
