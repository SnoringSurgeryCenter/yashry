<?php

namespace App\Http\Controllers\Api\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Website\BillResource;
use App\Http\Requests\Api\Website\BillStoreRequest;


class BillController extends Controller
{
    public function store(BillStoreRequest $request){
        $bill = $request->storeBill();
        return response([
            'message'   => 'created successfully',
            'bill'      =>  new BillResource ($bill),
        ]);

    }
    
}
