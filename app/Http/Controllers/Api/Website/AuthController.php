<?php

namespace App\Http\Controllers\Api\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Website\UserResource;
use App\Http\Requests\Api\Website\LoginRequest;

class AuthController extends Controller
{
    /**
     * 
     *
     * @param  App\Http\Requests\Api\Admin\LoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request)
    {
        $user = $request->login();
        $token = $user->createToken('yashry-token')->accessToken;
        return response([
            'message' => 'Successfully logged in',
            'token' => $token,
            'user' => new UserResource($user),
        ]);
    }

    /**
     * Salezagee user logout.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        auth('api')->user()->token()->delete();
        return response([
            'message' => 'Successfully logged out',
        ]);
    }
}
