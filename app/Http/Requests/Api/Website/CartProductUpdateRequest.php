<?php

namespace App\Http\Requests\Api\Website;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Models\Cart;
use Auth;

class CartProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->cart = Cart::getInstance();
        return [
            'product_id' => ["required","exists:products,id",
            function ($attribute, $value, $fail) {
                if (! optional($this->cart->products())->where('product_id', $value)->first() )   {
                    $fail("Product is not in cart to update!");
                }
            }],
            'count'      => 'required|numeric'
        ];
    }

    public function updateCartProduct()
    {
        $this->cart->products()->updateExistingPivot($this->product_id,[
                 'count' => $this->count
             ]);
       
        return $this->cart->refresh();
    }
}
