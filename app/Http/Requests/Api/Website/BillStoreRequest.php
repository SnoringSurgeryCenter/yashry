<?php

namespace App\Http\Requests\Api\Website;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\Services\billService;
use Auth;

class BillStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'currency_id' => 'required|exists:currencies,id',
        ];
    }

    public function storeBill()
    {
        $bill_service = new BillService($this->currency_id);
        $bill = $bill_service->setSubTotal()
                    ->setTaxes()
                    ->createBill();
        return $bill->refresh();
    }
}
