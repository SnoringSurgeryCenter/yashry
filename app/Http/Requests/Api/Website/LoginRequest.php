<?php

namespace App\Http\Requests\Api\Website;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->user = User::where('email', $this->email)->first();
        return [
            'email' => 'required|email|exists:users,email',
            'password' => [
                'required',
                'string',
                'max:191',
                function ($attribute, $value, $fail) {
                    if (! Hash::check($value, optional($this->user)->password)
                    ) {
                        $fail('Wrong credentials');
                    }
                },
            ],
        ];
    }

    public function login()
    {
        return $this->user;
    }
}
