<?php

namespace App\Http\Resources\Website;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Website\CartProductResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'   => $this->name,
            'email'  => $this->email,
            'cart'   => CartProductResource::Collection($this->cart->cartproducts()->get() )
               ];
    }
}
