<?php

namespace App\Http\Resources\Website;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Website\ProductSimpleResource;
class DiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name,
            'value'  => $this->value,
            'value_type'  =>$this->value_type,
            'count_product'  => $this->count_product,
            'discounted_product'  => new ProductSimpleResource($this->discountedProduct),
            'is_available'        => $this->is_available
        ];
    }
}
