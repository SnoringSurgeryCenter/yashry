<?php

namespace App\Http\Resources\Website;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Website\BillDiscountResource;
class BillResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'sub_total'   => $this->sub_total,
            'taxes'       => $this->taxes,
            'bill_discounts'  =>BillDiscountResource::Collection($this->billDiscounts()->get()),
            'total'  => $this->total,
        ];
    }
}
