<?php

namespace App\Http\Resources\Website;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Website\BillDiscountResource;

class BillDiscountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'discount'    => new DiscountSimpleResource($this->discount),
            'value'       => $this->value
        ];
    }
}
