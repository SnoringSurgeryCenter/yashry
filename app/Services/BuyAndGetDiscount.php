<?php 
namespace App\Services;

class BuyAndGetDiscount implements DiscountInterface {
    public function getDiscountValue($bill, $discount, $product, $discountedAmount) {
        $discountedAmount = $discountedAmount + $discount->getDiscountedAmount($discount->discountedProduct->price);
        $value = $bill->currency->change ($discount->getDiscountedAmount($discount->discountedProduct->price) );
        return $value;
    }
}