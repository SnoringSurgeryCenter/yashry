<?php 
namespace App\Services;

class SimpleDiscount implements DiscountInterface {
    public function getDiscountValue($bill, $discount, $product, $discountedAmount) {
        $discountedAmount = $discountedAmount +  $discount->getDiscountedAmount($product->price);
        $value = $bill->currency->change( $discount->getDiscountedAmount($product->price) );
        
        return $value;
    }
}