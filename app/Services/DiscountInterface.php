<?php 
namespace App\Services;
use App\Models\Discount;
use App\Models\Bill;
use App\Models\Product;

interface DiscountInterface{
    public function getDiscountValue(Bill $bill, Discount $discount, Product $product, $discountedAmount);
}