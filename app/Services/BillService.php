<?php

namespace App\Services;
use App\Models\Bill;
use App\Models\Setting;
use App\Models\Currency;
use App\Models\Cart;
use App\Services\SimpleDisount;
use App\Services\BuyAndGetDiscount;
use Auth;

class BillService
{
    protected $bill;
    protected $currency;
    protected $total;
    protected $discount_strategy;
    protected $sub_total;
    protected $taxes;
    
    public function __construct($currency_id)
    {
        $this->currency = Currency::Find($currency_id);
        $this->bill = new Bill();
        $this->discount_strategy = NULL;
        $this->sub_total = 0;
        $this->taxes = 0;
        
    }
    
    public function createBill(){
        $this->bill = Auth::user()->bills()->create([
            'currency_id' => $this->currency->id,
            'sub_total'     => $this->sub_total,
            'taxes'         => $this->taxes,
            'total'         => $this->sub_total + $this->taxes
    
        ]);
        $this->getDiscounts();
        return $this->bill;
    }
    public function setSubTotal(){
        $cart = Cart::getInstance();
        $sub_total= 0;
        foreach($cart->products as $product){
            $sub_total += ( $product->price * $product->pivot->count ) ;
        }
        $this->sub_total = $this->currency->change($sub_total);
        return $this;
    }
    public function setTaxes(){
        $taxes = (float) Setting::key('Taxes')->value;
        $this->taxes =  $this->sub_total *($taxes/100);
        return $this;
    }
    private function getDiscounts(){
        $discountedAmount = 0;
        $cart = Cart::getInstance();
        foreach($cart->products as $product){
            $discount = $product->getEligableDiscount($product->pivot->count);
            if($discount){

                if($discount->discounted_product_id == $product->id)    // 10 % off ($this->type)
                    $this->discount_strategy = new SimpleDiscount();
                else{                                                   //buy x of product 1 get discount on product 2
                    if( $cart->products()->where('product_id',$discount->discounted_product_id))
                        $this->discount_strategy = new BuyAndGetDiscount();
                }

                if($this->discount_strategy != NULL){
                    $discountedAmount = $discountedAmount + $this->applyDiscount( $discount, $product , $discountedAmount);
                }
                
            }
        }
        $this->bill->deductDiscountAmount($this->currency->change( $discountedAmount ));
    }

    private function applyDiscount($discount, $product, $discountedAmount){
        $value = $this->discount_strategy->getDiscountValue( $this->bill, $discount, $product, $discountedAmount );
        $this->bill->discounts()->attach($discount->id,[
            'value'       => $value
            ]);
        return $value;
    }
}
