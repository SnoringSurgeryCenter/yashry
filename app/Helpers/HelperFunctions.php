<?php
if (! function_exists('paginationLength')) {
    function paginationLength($modelName, $length = 20) {
        $paginationLength = request()->pagination;
        $model = classNameOf($modelName, true);
        if ($paginationLength == 'all') {
            $modelRaws = $model::count();
            return $modelRaws <= 500 ? $modelRaws : 500 ;
        }
        if (is_numeric($paginationLength)) {
            return $paginationLength;
        }
        return (int) $length;
    }
}